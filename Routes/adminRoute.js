// for routes : '/admin/* '
module.exports = function(router, passport)
{
  var mkdirp = require('mkdirp');

  var adminPagePath = '';
  var mongoose = require('mongoose');
  var servicesModel = require('../models/servicemodel.js'); //holds various services
  var messagesModel = require('../models/messageModel.js');
  //------------------------------------MULTER setup-------------------------------------------------------
  var multer = require('multer');
  var storage = multer.diskStorage({
    //destination is used to determine within which folder the uploaded files should be stored.

    //NOTE : We are passing function to destionation => its our responsibility to create the directory first as multer won't be creating it.
    destination: function(req, file, cb){
      console.log("DESTIONATION FUNCTION CALLED");
      if(!req.body.serviceName){
        console.log("DESTINATION ERROR : No service name");
        return cb(new Error(" destination error : No Service Name field found in request body."));
      }


      let filePath = "./public/images/services/"+req.body.serviceName;//Save image to folder
      //Create directory if not already created
      mkdirp(filePath, function(err){
        console.log("mkdirp : > attempt to create directory");
        if(err){
          console.log("mkdirp > failed to create directory");
          return cb(err);
        }else cb(null, filePath);
      });
    },

    //filename is used to determine what the file should be named inside the folder.
    filename : function(req, file, cb){
      console.log("FILENAME FUNCTION CALLED");
      console.log(JSON.stringify(file));
      if(!req.body.serviceName){
        console.log("FILENAME ERROR : No Service Name");
        return cb(new Error("filename error : No Service Name"));
      }
      cb(null, Date.now()+'_'+file.originalname); //image files
    }
  });
  var upload = multer({storage:storage});//.single('serviceImagePath');
  //--------------------------------END OF MULTER SETUP--------------------------------------------------------------



  //service model contains services (1 service holds multiple items)

  // route : /admin
  router.get('/', function(req, res){
    if(req.user){
      res.redirect("/admin/adminpanel");
    }
    else res.render(adminPagePath+'adminPanel.ejs', {locals:{samePage : false}});
  });

  // route : /admin
  router.post('/', passport.authenticate('local'),function(req, res){ //if fxn called => success authentication
    console.log("authentication successful "+JSON.stringify(req.user));
    res.status(200).redirect('/admin/adminpanel');
  });

  //route /admin/adminPanel
  router.get('/adminpanel', function(req, res){
    if(req.user){
      res.render(adminPagePath+'adminControlPanel.ejs', {locals:{samePage:false}});
    }
    else res.status(200).redirect('/admin');
  });

  //route : /admin/adminPanel/landingPageManager () : Holds services,
    //this route will query the database for services and then render the page,
  router.get("/adminpanel/landingPageManager", function(req, res){
    if(req.user)
    {
      servicesModel.find({}, function(err, docs){
        //docs hold multiple services {each service holds : |NAME| DETAIL | IMAGEPATH | [arrayOfItemsOffered]  }
        res.status(200).render(adminPagePath+"landingPageManager", {locals:{samePage:false, services:docs}});
      });
      }else res.redirect('/admin');
  });

  router.get("/adminpanel/servicePageManager", function(req, res){
    if(req.user){
      servicesModel.find({}, function(err, docs){
        res.status(200).render("servicePageManager", {locals:{samePage:false, services:docs}});
      });
    }
    else res.redirect('/admin');
  });

  router.get("/adminpanel/inbox", function(req, res){
    if(req.user){
      messagesModel.find({}, function(err, docs){
        res.status(200).render("inbox",{locals:{samePage:false, messages:docs}});
      });
    }
    else res.redirect('/admin');
  });

  //async ajax response, no re-direct should be initiated here, only res.send or not, based on if user is valid or not
  router.get("/adminpanel/landingPageManager/:id", function(req, res){
    if(req.user){
      var serviceId = req.params.id;
      servicesModel.getServiceById(serviceId, function(err, obj){
        if(err){
          res.status(400).send({message:"Invalid Service ID, no such id exist in database"});
        }else{
          res.status(200).send(obj);
        }
      });

    }else{
      res.status(400).send({message : "UnAuthorized User"});
    }
  });

  //route : /admin/adminpanel/landingPageManager : POST (add service)
  router.post("/adminpanel/landingPageManager", upload.none()/*upload.single('serviceImagePath')*/, function(req, res, next){
    // Temporarily disabled imageFile transfer via req.file and user should use google drive to upload image
    if(req.user)
    {
      //add new service item
      var newServiceData = req.body; //holds text file { serviceName and serviceDetail }
      /*
      var imageFile = req.file; //hols the image file { itemImagePath }
      var imageFilePath;
      if(imageFile){
        imageFilePath = req.file.path;
          if(imageFilePath.length != 0)
          //concatenate serviceData object with imageFile path
            newServiceData.serviceImagePath = new String(imageFilePath);
      }
      console.log("upload 2 : " + JSON.stringify(imageFile));
      */

      console.log('UPLOAD : '+JSON.stringify(newServiceData));
      servicesModel.createService(newServiceData, function(err, addedServiceObject){
        if(err){ //throws error if required fields not received
          console.log("error while upload : " + JSON.stringify(err));
          res.status(400).redirect("/admin/adminpanel/landingPageManager");
        }else{
          console.log("Information entered : "+ JSON.stringify(newServiceData));
          res.status(200).redirect("/admin/adminpanel/landingPageManager");
        }
      });
    }
    else res.status(200).redirect('/admin');
  });

  //update/edit request. This Request is not intended for ajax call, a form will submit data to this link and expects a redirection to "/landingPageManager"
  router.post("/adminpanel/landingPageManager/:id", upload.none(), function(req, res){
    if(req.user){
      var  selectedId = req.params.id;
      servicesModel.getServiceById(selectedId, function(err, doc){
        if(err){
          res.redirect('/admin/adminpanel/landingPageManager');
        }else{
        doc.set(req.body);
        doc.save(function(err, updatedDoc){
          return res.redirect('/admin/adminpanel/landingPageManager');
        });
      }
      });
    }else{
      //TODO : check the status to return in this case
      res.redirect('/admin');
    }
  });

  //route  : /admin/adminpanel/landingPageManager : DELETE (remove service)
  router.delete("/adminpanel/landingPageManager/:id", function(req, res){
    if(req.user){

      var newServiceData = req.body;
      var serviceId = req.params.id;
      //removeSerice method will also check if service exists or not
      servicesModel.removeService(serviceId, function(err, response){
        if(err){
            //service not found / error
            //TODO : status code
            res.redirect("/admin/adminpanel/landingPageManager");
        }
        else{
          //removed service from db
          res.status(200).redirect("/admin/adminpanel/landingPageManager");
        }
      });
    }
    else res.status(200).redirect('/admin');
  });

  //route : /admin/adminpanel/servicePageManager
  router.get("/adminpanel/servicePageManager", function(req, res){
    if(req.user){
      res.send("user : admin2010 > This page is still under development.");
    }else res.status(200).redirect('/admin');
  });

  //AJAX CALL TO GET ITEM
  router.get('/adminpanel/servicePageManager/:serviceId/:itemId', function(req, res){
    if(req.user){
      console.log("called ajax to get item");
      let serviceId = req.params.serviceId;
      let itemId = req.params.itemId;
      servicesModel.findServiceItemById(serviceId, itemId, function(err, doc){
        if(err)
          res.status(400).send(null);
        else{
          console.log("accepted request");
          res.status(200).send(doc);
        }
      });
    }else res.status(400).send(null);
  });


  //add new item to given service (serviceId)
  router.post("/adminpanel/servicePageManager/item/:serviceId", upload.none(),  function(req, res){
    if(req.user){
      var itemData = req.body;
      console.log("initiating request or : "+JSON.stringify(itemData));
      servicesModel.addNewServiceItem(req.params.serviceId, itemData, function(err, itemId){
        if(err){
          res.redirect('/admin/adminpanel/servicePageManager');
        }else{
          console.log("no error : item id is : "+itemId);
          res.redirect('/admin/adminpanel/servicePageManager');
        }
      });
    }
    else res.redirect('/admin');
  });

  //delete items
  router.delete('/adminpanel/servicePageManager/:serviceId/:itemId', function(req, res){

    if(req.user)
    {
      let serviceId = req.params.serviceId;
      let itemId = req.params.itemId;
      servicesModel.deleteServiceItem(serviceId, itemId, function(err){
        if(err){
          console.log("failed to delete item");
          res.redirect('/admin/adminpanel/servicePageManager');
        }
        else res.redirect('/admin/adminpanel/servicePageManager');
      });
    }
    else res.redirect('/admin');
  });

  //update existing item (EDIT)
  router.post('/adminpanel/servicePageManager/:serviceId/:itemId', upload.none(), function(req, res){
    if(req.user){
      let serviceId = req.params.serviceId;
      let itemId = req.params.itemId;
      servicesModel.getServiceById(serviceId, function(err, serviceDoc){
        serviceDoc.serviceItemsList.id(itemId).set(req.body);
        serviceDoc.save(function(err, up){
          console.log("saving : "+up);
          res.redirect('/admin/adminpanel/servicePageManager');
        });
      });
    }
    else res.redirect('/admin');

  });

  //inbox

  //AJAX handler
  router.get("/adminpanel/inbox/message", function(req, res){
    if(req.user){
      console.log("Attempt");
      var messageArray = new Array();
      messagesModel.find({}, function(err, docs){
        docs.forEach(function(element){
          messageArray.push(element);
        });

        console.log(messageArray.length);
        if(messageArray.length > 0)
        {
            messageArray.sort(function(a,b){
              return (b.senderTime - a.senderTime);
            });
            res.send({locals:{messages:messageArray}});
        }
        else res.send({locals:{err:new Error("No Data Found")}});
      });
    }
    else res.send({locals:{err:new Error("UnAuthorized")}});
  });

  //AJAX HANDLER
  router.delete("/adminpanel/inbox/message/:messageId", function(req, res){
    if(req.user){
      messagesModel.removeMessage(req.params.messageId, function(err, doc){
          res.redirect('/admin/adminpanel/inbox/');
      });

    }else res.redirect('/admin');
  });
}
