
var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
  senderName : String,
  senderEmail : String,
  senderPhone : String,
  senderMessage : String,
  senderDate : [String], // DD:MM:YYYY (size of array is 3)
  senderTime : Number
});

//defining custom static methods on model
messageSchema.static({
  addMessage( messageObject, cb)
  {
    let D = new Date();
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"];

    messageObject.senderDate = new Array(3);
    messageObject.senderDate[0] = D.getDate();
    messageObject.senderDate[1] = months[D.getMonth()];
    messageObject.senderDate[2] = D.getFullYear();

    messageObject.senderTime = D.getTime();

    var newUserMessage = new this(messageObject);
    newUserMessage.save(function(err, msg){
      if(!err){
        console.log("message created successfully ");
      }
      return cb(err, msg);
    });
  },
  removeMessage(messageId, cb){
    this.findByIdAndRemove(messageId, function(err){
      if(err){
        return cb(err);
      }
      return cb(err, {message : "successfully Removed"});
    });
  },
  getMessageById(messageId, cb){
    this.findById(messageId, function(err, doc){
      return cb(err, doc);
    });
  }
}); //end of static meethodds


module.exports = mongoose.model("Message", messageSchema);
