var mongoose = require('mongoose');

//---------------------- SERVICE ITEMS -----------------------
var serviceItemSchema = new mongoose.Schema({
  itemImagePath : String,
  itemName : String,
  itemDetail : String
});
//defining custom instance method (used upon documents)
serviceItemSchema.method({
  //@params : String
  updateImagePath(newPathString){
    this.itemImagePath = newPathString;
  },
  //@params : String
  updateName(newNameString){
    this.itemName = newNameString;
  },
  //@params : String
  updateDetail(newDetailString){
    this.itemDetail = newDetailString;
  },
  //@params : Object
  replaceItem( newItemObject ){
    this.itemImagePath = newItemObject.itemImagePath;
    this.itemName = newItemObject.itemName;
    this.itemDetail = newItemObject.itemDetail;
  },
  getItemId(){
    return this._id;
  }
});

//-----------------SERVICES----------------------
var serviceSchema = new mongoose.Schema({
  serviceDetail : String,
  serviceName : String,//heading text, this will be displayed on page
  serviceImagePath : String,
  serviceCountry : String,
  serviceItemsList : [serviceItemSchema]

});
//defining custom instance method (used upon 1 single service)
serviceSchema.method({
  addNewItem(newItemObject)
  {
    console.log("adding item : "+JSON.stringify(newItemObject));
    this.serviceItemsList.push(newItemObject);
    var newlyAddedItem = this.serviceItemsList[this.serviceItemsList.length-1];
    var newlyAddedItemId = newlyAddedItem._id;
    return newlyAddedItemId;
  },
  getServiceId(){
    return this._id;
  },
  getServiceName(){
    return this.serviceName;
  },
  getServiceDetail(){
    return this.serviceDetail;
  },
  getServiceImagePath(){
    return this.serviceImagePath;
  }
});

//defining custom static methods on model
serviceSchema.static({
  //call this method to create a new service (1 service hold multiple service items)
  createService( newServiceObject, cb)
  {
    console.log("SERVICE MODEL : attempt to create a new service : " + newServiceObject.serviceName);
    if(!newServiceObject.serviceName){
      console.log("not saving in database");
      return cb(new Error("No Service Name exist"));
    }
    var newService = new this(newServiceObject);
    newService.save(function(err, newlyAddedService){
      if(!err){
        console.log("service created successfully ");
      }
      return cb(err, newlyAddedService);
    });

  },

  //Add new item : Completed
  addNewServiceItem( serviceId, serviceItemObject, cb){
    console.log("call to add new item : "+JSON.stringify(serviceItemObject));
    this.getServiceById(serviceId, function(err, serviceDocument){
      if(err){
        return cb(err);
      }
      //add new item to service document and save the document
      var addedItemId = serviceDocument.addNewItem(serviceItemObject);
      console.log("Item added : "+addedItemId);
      serviceDocument.save(function(err, updatedDocument){
        if(err)
          return cb(err);
        return cb(err, addedItemId);
      });
    });
  },

  deleteServiceItem(serviceId, serviceItemId, cb){
    this.getServiceById(serviceId, function(err, serviceDoc){
      if(err)
        return cb(err);
      serviceDoc.serviceItemsList.id(serviceItemId).remove();
      serviceDoc.save(function(err){
        return cb(err);
      });
    });
  },

  findServiceItemById(serviceId, serviceItemId, cb){
    this.getServiceById(serviceId, function(err, serviceDoc){
      if(err)
        return cb(err);

      let item = serviceDoc.serviceItemsList.id(serviceItemId);

      if(item)
        return cb(null, item);
      return cb(new Error("no such item found in the service."));
    });
  },

  //remove service from model : Completed
  removeService(serviceId, cb){
    //implicitly calls model save() : see web for more details
    this.findByIdAndRemove(serviceId, function(err){
      if(err){
        return cb(err);
      }
      return cb(err, {message : "successfully Removed"});
    });
  },

  getServiceById(serviceId, cb){
    this.findById(serviceId, function(err, doc){
      return cb(err, doc);
    });
  }
}); //end of static meethodds

  //construct model
  module.exports = mongoose.model("Service", serviceSchema);
