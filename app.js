var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
const uuidv1 = require('uuid/v1');
var sendEmail = require('gmail-send')({user : 'skgoindia@gmail.com', pass:'3313rajan', to : 'skgoindia@gmail.com'});
var nodemailer = require('nodemailer');
var session = require('express-session');
var mongoose = require('mongoose');
var multer  = require('multer');

var methodOverride = require('method-override');

const MongoStore = require('connect-mongo')(session);
//mongoose.Promise= require("bluebird");
var passport = require('passport');//passportjs for user auth
var LocalStrategy = require('passport-local').Strategy;
const passportLocalMongoose = require('passport-local-mongoose');
const port = process.env.PORT || 3000;

var app = express();
var httpServer = http.createServer(app);

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(methodOverride('_method'))
app.set('views', [path.join(__dirname, 'views'),
  path.join(__dirname, 'views/adminPages/')]);
app.use(express.static("Routes"));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static("public"));

app.use(session({
    secret : "skgoindia secret code",
    resave : false,
    name : 'skgoindia_Sess',
    saveUninitialized : false,
    genid : function(req){return uuidv1();},
    store: new MongoStore({mongooseConnection : mongoose.connection}),
    cookie:{
      httpOnly:true,
      maxAge: 3600000
    }
  }
));
app.use(passport.initialize());
app.use(passport.session());

app.set("view engine", "ejs");
app.set('trust proxy', true);

var transporter = nodemailer.createTransport({
  service : 'gmail',
  auth:{
    user:'skgoindia@gmail.com',
    pass:'rajanshikha'
  }
});

const mongoOption = {
  useNewUrlParser : true,
  useCreateIndex: true
};
mongoose.connect('mongodb://skgoindia:rajan2010@ds052978.mlab.com:52978/sktravelwebsite', mongoOption,function(err){
  if(err)
    console.log("COULD NOT CONNECT : "+err);
  else console.log("CONNECTED TO MONGODB");
});
var passportLocalMongooseOptions = {
    errorMessages: {
        MissingPasswordError: 'Custom : No password was given',
        AttemptTooSoonError: 'Account is currently locked. Try again later',
        TooManyAttemptsError: 'Account locked due to too many failed login attempts',
        NoSaltValueStoredError: 'Authentication not possible. No salt value stored',
        IncorrectPasswordError: 'Password or username are incorrect',
        IncorrectUsernameError: 'Password or username are incorrect',
        MissingUsernameError: 'No username was given',
        UserExistsError: 'A user with the given username is already registered'
    }
};

var adminSchema = new mongoose.Schema({});
adminSchema.static({
  getTotalAdmin(){
    this.count({}, function(err, count){
      if(!err) return count;
      return 0;
    })
  }
});
adminSchema.plugin(passportLocalMongoose, passportLocalMongooseOptions);

var adminModel = mongoose.model('Admin', adminSchema);
passport.use(new LocalStrategy(adminModel.authenticate()));
passport.serializeUser(adminModel.serializeUser());
passport.deserializeUser(adminModel.deserializeUser());

console.log("APP.JS > ATTEMPTING TO REGISTER USER");

var queryPromise = adminModel.findOne({username:'admin2010'}).exec();
queryPromise.then(
  function(user){
    if(user){
      console.log("ADMIN ACCOUNT ALREADY IN DATABASE : "+JSON.stringify(user));
    }
    else{
      adminModel.register( {username:"admin2010"}, "admin15157", function(err, user){
        if(err){
          console.error(" FAILED TO REGISTER ADMIN ACOUNT "+err);
        }
        console.log("ADMIN ACCOUNT REGISTERED SUCCESSFULLY");
      });
    }
  },
  function(err){ //otherwise create
  adminModel.register( {username:"admin2010"}, "admin15157", function(err, user){
    if(err){
      console.error(" FAILED TO REGISTER ADMIN ACOUNT "+err);
    }
    console.log("ADMIN ACCOUNT REGISTERED SUCCESSFULLY");
  });
});


var adminRouter = express.Router();
require('./Routes/adminRoute.js')(adminRouter, passport);
app.use('/admin', adminRouter);

//ROUTES DEFINATION START
app.get("/", function(req, res){
  require('./models/servicemodel.js').find({}, function(err, docs){
      if(err){
        res.send("Unable to serve this request, Try again later.");
      }else{
        res.render("landing", {locals:{samePage:true, services:docs}});
      }
  });
});

var messageModel = require('./models/messageModel.js');

//When customer attempts to submit contact us form data
app.post("/", function(req, res){
  var userData = req.body;

  let mailOptions = {
    from : userData.senderEmail, //sender's email
    to : 'skgoindia@gmail.com',
    subject : 'SKGOINDIA.COM > New Message from ' + userData.senderName,
    html : '<p> <b> Sender Name : </b> ' + userData.senderName + ' <br> <b> Phone : </b>' + userData.senderPhone + '<br> <b> Email : </b> : '+ userData.senderEmail + '<br> <b> Message : </b> ' + userData.senderMessage
  };
  //first send to gmail
  transporter.sendMail(mailOptions, function(err, info){
    if(err){
      console.log("Failed to send to gmail " + err);
      //do nothing, pretty sure message will also make it way to backend personal database
    }else{
        console.log("Sent to gmail");
    }
  });

  messageModel.addMessage(userData, function(err, response){
    if(err){
      res.render("landing.ejs",{ locals:{ type : 'formSubmit',samePage : true,success : false,text : "Unable to send your message due to technical difficulty."}});
    }else{
      console.log("saved to database : "+response);
      res.render('landing.ejs',{locals:{type : 'formSubmit', samePage : true,success:true,text : "Success!"}});
    }
  });
});

app.get("/sportscar", function(req,res){
  res.render("sportsCarService", {locals:{samePage : false}});
});
app.get("/aviation", function(req, res){
  res.render("aviationService", {locals:{samePage : false}});
});
app.get("/vintage", function(req, res){
  res.render("vintageCarService", {locals:{samePage : false}});
});
app.get("/ultraluxury", function(req, res){
  res.render("ultraLuxuryService", {locals:{samePage : false}});
});
app.get("/luxury", function(req, res){
  res.render("luxuryService", {locals:{samePage : false}});
});
app.get("/premiumcompactLuxury", function(req, res){
  res.render("premiumCompactLuxury", {locals:{samePage : false}});
});
app.get("/compactsedan", function(req, res){
  res.render("compactSedan", {locals:{samePage : false}});
});
app.get('/convertibles', function(req, res){
  res.render('convertible.ejs', {locals:{samePage : false}});
});
//END OF ALL ROUTES DEFINATION

httpServer.listen(port, ()=>{
  console.log("Application Started on port "+port);
});
